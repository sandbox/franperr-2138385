<?php

/**
 * Implements hook_permission().
 */
function bypass_required_fields_permission() {
  $nodetypes = node_type_get_names();
  // We define for each node type a permission to assign to users.
  foreach ($nodetypes as $machinename => $humanname) {
    $permissions['bypass_required_fields for new ' . $machinename] = array(
      'title' => t('<em>@nodetype</em>: bypass required fields for <em>new</em> nodes.', array('@nodetype' => $humanname)),
    );
    $permissions['bypass_required_fields for existing ' . $machinename] = array(
      'title' => t('<em>@nodetype</em>: bypass required fields for <em>existing</em> nodes.', array('@nodetype' => $humanname)),
    );
  }
  return $permissions;
}

/**
 * Implements hook_form_alter().
 */
function bypass_required_fields_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#node_edit_form'] == TRUE && empty($form['nid']['#value'])) {
    $node_type = $form['#node']->type;
    if (user_access('bypass_required_fields for new ' . $node_type)) {
      // Current user as the permission by_required_fields set for this
      // node type. So we set a drupal message to warn and a custom
      // validation to skip form errors.
      drupal_set_message(t('<b>You are bypassing required fields for this new node</b>'), 'warning');
      $form['#validate'][] = '_bypass_required_fields_skip_required_validate';
    }
  }
  if ($form['#node_edit_form'] == TRUE && !empty($form['nid']['#value'])) {
    $node_type = $form['#node']->type;
    if (user_access('bypass_required_fields for existing ' . $node_type)) {
      // Current user as the permission by_required_fields set for this
      // node type. So we set a drupal message to warn and a custom
      // validation to skip form errors.
      drupal_set_message(t('<b>You are bypassing required fields for this existing node</b>'), 'warning');
      $form['#validate'][] = '_bypass_required_fields_skip_required_validate';
    }
  }
}

/**
 * Implements hook_form_validate().
 */
function _bypass_required_fields_skip_required_validate($form, &$form_state) {
  $errors = form_get_errors();
  if ($errors) {
    // Clear errors.
    form_clear_error();
    // Clear error messages.
    $error_messages = drupal_get_messages('error');

    // We remove only errors linked to fields.
    $removed_messages = array();
    foreach ($errors as $name => $error_message) {
      if (substr($name, 0, 6) == 'field_') {
        $removed_messages[] = $error_message;
        unset($errors[$name]);
      }
    }
    // Reinstate remaining errors.
    foreach ($errors as $name => $error) {
      form_set_error($name, $error);
      // form_set_error() calls drupal_set_message(), so we have to filter out
      // these from the error messages as well.
      $removed_messages[] = $error;
    }
    // Reinstate remaining error messages (which, at this point, are messages
    // that were originated outside of the validation process).
    foreach (array_diff($error_messages['error'], $removed_messages) as $message) {
      drupal_set_message($message, 'error');
    }
  }
}
