This module will create a "bypass required field" role for each type of
node. The users with this role will be able to bypass the required field
when creating/editing nodes of the corresponding type.
The module will show a warning above the form about the
"non-requirement" of fields.
This is useful if you edit old nodes when you have change the required
fields in the meantime or if somebody who doesn't have the information
is editing nodes.
